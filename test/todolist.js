const DDToken = artifacts.require("DDToken");

contract("DDToken", (accounts)=> {
    let [bob, alice] = accounts;
    it("should display total  supply", async ()=>{
        const ddCoinInstance = await DDToken.new(1000, "dd", 10, "DD");
        const balance = await ddCoinInstance.totalSupply();
        assert.equal(balance.valueOf(), 1000, "1000 was not send to bob");
    });

    it("should put 1000 coins in bob's account", async ()=>{
        const ddCoinInstance = await DDToken.new(1000, "dd", 10, "DD");
        const balance = await ddCoinInstance.balanceOf.call(bob);
        assert.equal(balance.valueOf(), 1000, "1000 was not send to bob");
    });

    it("should transfer coins using the transfer function from bob to alice", async ()=>{
        const ddCoinInstance = await DDToken.new(1000, "dd", 10, "DD");
        await ddCoinInstance.transfer(alice, 10, {from: bob});
        const balance = await ddCoinInstance.balanceOf.call(alice);
        assert.equal(balance.valueOf(), 10, "10 coins was not send to alice");
    });

    it("should make bob to approve alice to transfer coins ", async ()=>{
        const ddCoinInstance = await DDToken.new(1000, "dd", 10, "DD");
        await ddCoinInstance.approve(alice, 10, {from: bob});
        const allowance = await ddCoinInstance.allowance.call(bob, alice);
        assert.equal(allowance.valueOf(), 10, "10 coins was not allowed to be send from bob to alice");
    });

    it("should transfer coins using the transferFrom function from bob to alice", async ()=>{
        const ddCoinInstance = await DDToken.new(1000, "dd", 10, "DD");
        await ddCoinInstance.approve(alice, 10, {from: bob});
        await ddCoinInstance.transferFrom(bob, alice, 10, {from: alice});
        const balance = await ddCoinInstance.balanceOf.call(alice);
        assert.equal(balance.valueOf(), 10, "10 coins was not send to alice");
    });
})